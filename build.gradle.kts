version = "1.0-SNAPSHOT"

plugins {
    java
    application
    id("org.springframework.boot") version "2.1.1.RELEASE"
    id("com.github.spotbugs") version "1.6.8"
}

repositories {
    mavenCentral()
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}

application {
    mainClassName = "jadle.WebFluxApplication"
}

dependencies {
    implementation(platform("org.springframework.boot:spring-boot-dependencies:2.1.1.RELEASE"))
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-actuator")

    implementation("io.micrometer:micrometer-core")
    implementation("io.micrometer:micrometer-registry-prometheus")

    implementation(platform("org.apache.logging.log4j:log4j-bom:2.6.1"))
    implementation("org.apache.logging.log4j:log4j-api")
    implementation("org.apache.logging.log4j:log4j-core")

    implementation(platform("com.fasterxml.jackson:jackson-bom:2.9.8"))
    implementation("com.fasterxml.jackson.core:jackson-databind")

    testImplementation(platform("org.junit:junit-bom:5.3.2"))
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}
