package jadle;

import org.reactivestreams.Publisher;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
class JadleRestController {

    private final Flux<String> timer = Flux.create(this::emitter);

    @PostConstruct
    public void start() {
        executor.scheduleWithFixedDelay(() ->
                emit(Instant.now().toString()), 1, 1, TimeUnit.SECONDS);
    }

    @GetMapping(path = "/events", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    Publisher<String> listen() {
        return timer;
    }

    private static final ScheduledExecutorService executor
            = Executors.newSingleThreadScheduledExecutor();
    private Collection<FluxSink<String>> emitters = new CopyOnWriteArrayList<>();

    private void emitter(FluxSink<String> emitter) {
        emitters.add(emitter);
        emitter.onDispose(() -> emitters.remove(emitter));
    }

    private void emit(String s) {
        for (FluxSink<String> sink : emitters) {
            sink.next(s);
        }
    }

    @GetMapping
    Publisher<String> getAll() {
        return Flux.just("x", "y");
    }

    @GetMapping("/{id}")
    Publisher<String> getById(@PathVariable("id") String id) {
        return Mono.just(id);
    }

}
